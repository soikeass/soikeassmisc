#!/bin/bash
set -e

# Script that waits for server to respond with http 200 for some time, on failure exits with code 1
#
# usage  : wait_for_server.sh HOST PORT MAXTRIES WAITSEC
# sample : wait_for_server.sh host.com 80 100 5

WAIT_HOST=$1
WAIT_PORT=$2
WAIT_LOOPS=$3
SLEEP=$4

echo "wait for ${WAIT_HOST}:${WAIT_PORT}, loops: $WAIT_LOOPS, sleep $SLEEP sec..."
i=0
while ! nc $WAIT_HOST $WAIT_PORT >/dev/null 2>&1 < /dev/null; do
  i=`expr $i + 1`
  if [ $i -ge $WAIT_LOOPS ]; then
    echo "ERROR - ${WAIT_HOST}:${WAIT_PORT} still not reachable after $i tries, giving up" && exit 1
  fi
  echo ".....wait for ${WAIT_HOST}:${WAIT_PORT} attempt $i....."
  sleep $SLEEP
done
echo "${WAIT_HOST}:${WAIT_PORT} ready"
