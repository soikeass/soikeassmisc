#!/bin/bash
set -e

# Script that waits for url to respond with http 200 for some time, on failure exits with code 1
#
# usage  : wait_for_server.sh URL MAXTRIES WAITSEC
# sample : wait_for_server.sh http://host.com:80/path 100 5

WAIT_URL=$1
WAIT_LOOPS=${2:-100}
SLEEP=${3:-5}

echo "wait for ${WAIT_URL}, loops: $WAIT_LOOPS, sleep $SLEEP sec..."
i=0
until $(curl --output /dev/null --silent --globoff --fail ${WAIT_URL}); do
  i=`expr $i + 1`
  if [ $i -ge ${WAIT_LOOPS} ]; then
    echo "ERROR - ${WAIT_URL} still not reachable after $i tries, giving up" && exit 1
  fi
  echo ".....wait for ${WAIT_URL} attempt $i....."
  sleep ${SLEEP}
done
echo "${WAIT_URL} ready"
