#!/bin/bash
set -e

# Script that waits for file to exist, on failure exits with code 1
#
# usage  : wait_for_file.sh FILE MAXTRIES WAITSEC
# sample : wait_for_file.sh /some/file.txt 100 5

WAIT_FILE=$1
WAIT_LOOPS=${2:-100}
SLEEP=${3:-5}

echo "wait for ${WAIT_FILE}, loops: $WAIT_LOOPS, sleep $SLEEP sec..."
i=0
while [ ! -f $WAIT_FILE ]; do
  i=`expr $i + 1`
  if [ $i -ge ${WAIT_LOOPS} ]; then
    echo "ERROR - ${WAIT_FILE} still not existing after $i tries, giving up" && exit 1
  fi
  echo ".....wait for ${WAIT_FILE} attempt $i....."
  sleep ${SLEEP}
done
echo "${WAIT_FILE} ready"
